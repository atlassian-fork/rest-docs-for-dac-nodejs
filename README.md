# REST API Beautifier for DAC - NodeJS version

This is a simple hacky way to beautify the Atlassian REST API docs 
for DAC.

Simplified fork of the Ruby version at https://bitbucket.org/rmanalan/rest-docs-for-dac.

Differences: 

* only supports a single local HTML file (no HTML downloading)
* supports specifying the target path, otherwise writes to `target`

## Usage

    $ npm install
    $ node index.js <path to rest doc>
    
### Arguments

*-d* or *--debug*: Write output to stdout, not to a file
*-p* or *--path*: Specify base path to be prepended to assets
*-t* or *--target* Specify target directory
