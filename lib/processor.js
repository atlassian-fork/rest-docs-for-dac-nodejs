var cheerio = require('cheerio');
var fs = require('fs');
var path = require('path');

module.exports = function processor(html, basePath) {
    var $header = cheerio.load(fs.readFileSync(path.normalize('./template/header.html')));
    var $footer = cheerio.load(fs.readFileSync(path.normalize('./template/footer.html')));

    var $ = cheerio.load(html);

    $('style').remove();

    var stylesheets = [
        "https://developer.atlassian.com/static/javadoc/jira/5.0.5/assets/doclava-developer-docs.css",
        basePath + "/assets/css/customizations.css",
        basePath + "/assets/css/rest.css"
    ];

    stylesheets
        .map(function (href) {
            return '<link rel="stylesheet" href="' + href + '">';
        })
        .forEach(function (link) {
            $('head').append(link);
        });

    var $body = $('body');
    var bodyHtml = $body.html();
    $header('.aui-header-logo-device').css('background-image', 'url(' + basePath + '/assets/images/atlassian-developers-logo.svg)');
    var $container = $('<div class="container">' + bodyHtml + '</div>');
    $body.empty()
        .append($header.html())
        .append($container)
        .append($footer.html());
    return $.html();
};