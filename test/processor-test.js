var cheerio = require('cheerio');
var expect = require('chai').expect;
var fs = require('fs');

var processor = require('../lib/processor');

function normalise(str) {
    return str.replace(/( {2,}|\t)/g, '');
}

describe('processor', function () {
   it('produces expected output for input', function() {
       var input = fs.readFileSync('test/fixtures/input.html');
       var output = processor(input, '../..');
       var expected = normalise(fs.readFileSync('test/fixtures/expected.html').toString());
       expect(normalise(output)).to.equal(expected);
   });
});